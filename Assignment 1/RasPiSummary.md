**RASPBERRY PI**
 
**INTRODUCTION:**
Raspberry pi is  a single computer board with credit card size,that
can be used for many tasks that our computer does.It  comes in
two models  i.e A and B . A  does not contain an Ethernet port
and B contains an Ethernet port.

**HARDWARE SPECS:**
 

- **CPU-**-This   is a Broadcom BCM2835 System on ma chip(SoC) that's made up of an ARM CPU and a Videocore 4 GPU.


- **Memory**--The raspberry pi model  is designed with 256 MB of SDRAM

⦁**GPIO pin**--These are exposed general purpose input/output connection pins.


- **RCA**--An RCA jack allows connection of analog TVs and other similar output devices.

⦁**Audio ou**t--There is a standard 3.55 mm jack for connectionof audio output devices.

⦁**HDMI**--This connector allows us to hook up a HD TV.

⦁**Powe**r-This is a 5V Micro USB power connector for power.

⦁**Etherne**t--This allows for wired network access.

⦁**SD cardslo**t--An SD card with an operating system(os) installed is required for booting the device.

⦁**UARC**--The universal Asynchronous Receiver/transmitter is a serial input and output port esed to transfer the serial data in form of text.

**WORKING WITH PI:**

⦁	 At  the  primary computer. Insert  SD card and format it according to the Foundation’s directions. This will install a 
     recovery program on it so we can save your card even if  we break it with your tinkering. 
  
⦁	Download NOOBS on our primary computer. Short for New Out Of Box Software, it’s the Raspberry Pi Foundation’s fittingly
    named distro for first-time Pi users. 
    
⦁	Load our NOOBS download onto the newly formatted SD card. 

⦁	Slide the SD card into the underside of the Raspberry Pi .Make sure it’s oriented correctly; it’d be bad to break our Pi before you turn it on!

⦁	The Raspberry Pi will boot up and takes us to the NOOBS screen. If it doesn’t, check the power supply and HDMI cables and make sure they’re secure.

⦁	Select an OS to install. .Pi  runs a version of Linux called Raspbian. Raspbian lets us nstall tons of software from it’s own open source software
    repository at no cost.

**Once the file copies, we’ll get a notice that says, “Image applied successfully.” Press return, and the Pi will reboot. **