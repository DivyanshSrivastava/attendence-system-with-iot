**SUMMARY**

"Live as if you were to die tomorrow,learn as if you were to live forever"-Mahatma Gandhi.

The quote,more aptly,the universal certitude seems to perfectly fitting in the box we are into
to look forwars to a better tomorrow.In this approx 20 mins of wheel work,this international
best selling author,an enterprenaur as well as a business coach,Josh Kaufman,unfolds the
rells os hos new life,more of a passionate job,and taking up new responsibilities as a father-
friendphilosopher,and at the same time managinghimself as  a grown up individual.Josh
unravels that how every new beginning comes from some other beginning as he potrays the 
journey of how thw birth of his infant,Leela made him a virtuoso,a more competent person
ih ths hectic all-daycrowded frenetic world.Learning is a correlated proces,precisely an
emotion,an inner interest,a train with no station to stop  at,because the more we have
passion,the more we have devotion.The more we devotr,the more we learn-the more we learn,
the better we develop,the faster we develop,the prone we are to inculcate,and once we 
inculcate,the closer we are to expertise,and finally we expertise,we excel and master 
ourselves,and this is how each day w grow.Its the life which never stops us teaching and
thus the process of learning continues.

Josh narrates that while his wife, an established online tutor and he as an author were busy
in their own career , the entrance of Leela made their vision for career to look in a diﬃcult 
mannner. It seemed that the perspective of their job was to earn for their little family 
and that he began magnifying each little thing he did for his tiny daughter. Not hiding his candid
thoughts that popped up in his head at times, like ‘never getting a free time again in his life’
or frowning expressions ‘how to dress up my daughter’he also at the same time reveals that 
he started enjoying his life with little Leela as he engaged himself in learning something new.
Dressing up his daughter in fascinating apparels and puddling around he eventually decided 
to solve the life puzzles of how to maximise learning and mastering a skill in the minimum 
time available. While surﬁng through the sites, doodling into stuﬀs and books all around to 
know ‘how long does it take to acquire a new skill’ he ultimately came across a stuﬀ of
10000 hrs by K.Anders Erricson of Statford which ﬁnally bursted him out with freaky
thoughts that he ‘would never be able to learn something new.’When he dived himself
deeper into the world of applied psychology he discovered the secrets behind the number
10,000 and uncoiled the fact that it was the ﬁnal stage expert level learning. Ofcourse 10000
hours is a not a joke...a 5 years 4times part time...and aﬀording it is something next to
impossible in bustling brisk frenzied life. We live in an era where we plump for smart work 
rather than hard work... and if we generalise the fact of 10000 hours, its rather a farcical or 
something not impossible but improbable for all of us. Josh ﬁnally invents his algo to decode
the puzzle by developing his tricks and techniques which eventually led him to a conclusion.
Being a geek, he makes us understand the concept through a simple graph of skill
acquisition, that learning something new,whether mental or physical, doesn’t take much 
time and it’s early time to acquire it is worthily proﬁcient. But the point of how perfect we 
are at the skill after we acquire it is inversely related to it. And so, to be ﬂawless at both of
the points and ﬁnally fetching up the to the constant part of it, at short time is what he 
unveiled in his nuggets of facts. The four modus operandi starting from deconstructing the
skills into smaller chunks, learning enough to self correct and self edit, removing practice
""barriers, more precisely, ﬁnding out excuses and distractions that refrain us from
practising, and ultimately the fourth knack, practising for minimum 20 hours would ﬁnally
ferry us to some soiled grounds. 

While he was towards the ending of sharing his experience, he cited his views through a
beautiful mesmerising example of how much passion he had to learn ukulele from his past.
Inspired by Jake Shimbakuro, he also opens up his areas of loving to play ukulele, somewhere 
deep delved in his heart far behind. Not ignoring the fact that it wasn’t so easy to play the 
ukulele and the journey has so many impedances of learning 100 notes to which would fetch 
him to play a good pop song , he bubbled up with an epilogue to learn only those four 
hardest notes which could make him play all the pop songs half way out. And this is how he 
presented a heart touching piece of his talent to his audience—-a song scripted by him,
more of an autobiography in his few lines ﬁlled with candidness and caramelised with happy
moments of his family, tossed with memories of love, and sprinkled with cute moments of 
anger and emotions, dedicated to his loving small family , and his tiny little Leela waiting
back for him at the house...Although the  frequencies of the ukulele and his vocals were not
so clearly matching, but the notes of his song seemed to touch the each cell of the rib cage of
his audience. He slowly unveils a secret that the song and the ukulele was his ﬁnal ‘20th’
hour practice and as he leaves the stage wishing everyone a funny life he leaves a thought in
each of his audience— an inner echo which says that yes, learning is a beautiful process, it
may not be so easy but it is very interesting, it makes each morning of our lives resplendent,
vibrant, full of sun beams and lush green hues... it’s something if mingled with passion and 
profession would not rather inspire for the end of the month to get the salary in our hands,
but rather motivate us to stay the whole life, to wake up each morning to work for it, to
master it in such a way without a parallel and to live happily smiling ever after.

