***....GITLAB....*****

**INTRODUCTION:-**


GitLab is a service that organizations can use to provide internal management of git repositories.
It is a self hosted Git-repository management system that keeps the user code private and can
easily deploy the changes of the code.


**TERMINOLOGY:-**


⦁**BLOB**:-Blob stands for Binary Large Object. . A blob holds the file data but doesn’t contain any
    metadata about the file.
    
⦁**TREE**:-Tree is an object, which represents a directory.  A tree is a binary file that stores
    references to blobs and trees which are also named as SHA1 hash of the tree object.
    

- **COMMIT**:-Commit holds the current state of the repository.


- **BRANCHES**:-Branches are used to create another line of development.Usually, a branch is created
    to work on a new feature.
    
⦁**TAG**:-Tag assigns a meaningful name with a specific version in the repository

⦁**CLONE**:-Clone operation creates the instance of the repository.

⦁**PUSH**:-Push operation copies changes from a local repository instance to a remote one.

⦁**WIKI**:-Wiki is a section for hosting documentation in Github.It is used to share long form content
    about our project.
    
⦁**ISSUES**:-It is the great way to keep track of  tasks,enhancements,and bugs for your project.

⦁**FORK:**-It is a sort of bridge between the original repository and your personal copy.Creating
    a “fork” is producing a personal copy of someone else's project.
 
